$(document).ready(function() {
    var btn = $('input:radio');
    var background = $('#background');
    var check = $('input:checkbox');

    btn.each(function () {
        if ($(this).attr('id')!= "radio-background1"){
            $(this).prop('checked', false);
            $('#radio-background1').prop('checked', true);
        }
        $(this).click(function () {
            if ($(this).attr("id").split('-')[1] == "background6"){
                background.attr("src", "../images/" + ($(this).attr("id").split('-')[1] + ".gif"));
            }
            else {
                background.attr("src", "../images/" + ($(this).attr("id").split('-')[1] + ".jpg"));
            }
        })
    });


    check.each(function() {
        var name = '#' + $(this).attr('id').split('-')[1];
        if ($(this).attr("id")!= "check-dolphin1"){
            $(this).prop('checked', false);
            $(name).hide();
            $('#check-dolphin1').prop('checked', true);
        }

        $(this).click(function () {
            if ($(this).is(':checked')){
                $(name).show();
            }
            else {
                $(name).hide();
            }
        })
    })

    var array = {"dolphin1": [300, 1],
                "dolphin2": [350, 70],
                "dolphin3": [150, 34],
                "dolphin4": [100, 60],
                "dolphin5": [250, 10],
                "dolphin6": [190, 50],
                "dolphin7": [180, 85],
                "dolphin8": [200, 10],
                }

    $('#vertical-control').val(50);
    $('#vertical-control').change(function () {

        var magnitude = (50 - $('#vertical-control').val())*5;

        var up = $('#vertical-control').val();

       $('.dolphin').each(function () {
           var name = $(this).attr('id');
           $(this).css('top',  array[name][0] + magnitude + 'px');
       })
    });

    $('#horizontal-control').val(50);
    $('#horizontal-control').change(function () {

        var magnitude2 = (50 - $('#horizontal-control').val());

        var up = $('#horizontal-control').val();

        $('.dolphin').each(function () {
            var name = $(this).attr('id');
            $(this).css('left',  array[name][1] + magnitude2 + '%');
        })
    });

    $('#size-control').val(50);
    $('#size-control').change(function () {

        var magnitude3 = ($('#size-control').val())*5;

        var up = $('#size-control').val();

        $('.dolphin').each(function () {
            var name = $(this).attr('id');
            $(this).css('width',  100 + magnitude3 + 'px');
        })
    });
})


